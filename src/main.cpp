#include "Matrix4x4.h"

int main()
{
	Vector3 vertices[4];

	vertices[0] = Vector3(0.0f, 0.0f, 0.0f);
	vertices[1] = Vector3(1.0f, 0.0f, 0.0f);
	vertices[2] = Vector3(0.0f, 0.0f, 1.0f);
	vertices[3] = Vector3(1.0f, 0.0f, 1.0f);

	Vector3 centre(0.0f);

	for (int i = 0; i < 4; ++i)
	{
		centre = centre + vertices[i];
		std::cout << "Vertex " << i << " : " << vertices[i] << std::endl;
	}

	centre = centre / 4.0f;

	std::cout << "Quad Centre: " << centre << std::endl;

	Matrix4x4 t;

	Vector3 U = vertices[1] - vertices[0];
	Vector3 W = vertices[2] - vertices[0];
	Vector3 V = Vector3::CrossProduct(W, U);

    // clang-format off
	t.m[0][0] = U.x;		t.m[0][1] = V.x;		t.m[0][2] = W.x;    t.m[0][3] =  centre.x;
	t.m[1][0] = U.y;		t.m[1][1] = V.y;		t.m[1][2] = W.y;    t.m[1][3] = centre.y;
	t.m[2][0] = U.z;		t.m[2][1] = V.z;		t.m[2][2] = W.z;    t.m[2][3] = centre.z;
	t.m[3][0] = 0.0f;	    t.m[3][1] = 0;	        t.m[3][2] = 0.0f;   t.m[3][3] = 1.0f;
    // clang-format on

	t.printMatrix();
	Vector3 p = Vector3(0.5, 0.0f, 0.5f);

	Vector3 newP = t * p;
    std::cout << "Original point: " << p << std::endl;
    std::cout << "Transformed point: " << newP << std::endl;

	return 0;
}