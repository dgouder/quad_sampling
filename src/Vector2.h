#pragma once

	class Vector2 {

	public:

		float x, y;

	public:
		Vector2() { x = y = 0.0f; }

		inline Vector2(float p_x, float p_y) : x(p_x), y(p_y)
		{
		}

        Vector2 operator*(float m)
        {
            return Vector2(x * m, y * m);
        }

        Vector2 operator+(Vector2 other)
        {
            return Vector2(x + other.x, y + other.y);
        }

	};
