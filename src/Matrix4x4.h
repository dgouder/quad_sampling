#include "Vector3.h"
#include <iostream>

struct Matrix4x4 {
	// Matrix4x4 Public Methods
	Matrix4x4() {
		m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.f;
		m[0][1] = m[0][2] = m[0][3] = m[1][0] = m[1][2] = m[1][3] = m[2][0] =
			m[2][1] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0.f;
	}

	inline Vector3 operator*(const Vector3 &pt) const {
		float x = pt.x, y = pt.y, z = pt.z;
		float xp = m[0][0] * x + m[0][1] * y + m[0][2] * z + m[0][3];
		float yp = m[1][0] * x + m[1][1] * y + m[1][2] * z + m[1][3];
		float zp = m[2][0] * x + m[2][1] * y + m[2][2] * z + m[2][3];
		float wp = m[3][0] * x + m[3][1] * y + m[3][2] * z + m[3][3];

		if (wp == 1.)
			return Vector3(xp, yp, zp);
		else
			return Vector3(xp, yp, zp) / wp;
	}

	bool operator==(const Matrix4x4 &m2) const {
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
				if (m[i][j] != m2.m[i][j]) return false;
		return true;
	}
	bool operator!=(const Matrix4x4 &m2) const {
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
				if (m[i][j] != m2.m[i][j]) return true;
		return false;
	}

	static Matrix4x4 Mul(const Matrix4x4 &m1, const Matrix4x4 &m2) {
		Matrix4x4 r;
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
				r.m[i][j] = m1.m[i][0] * m2.m[0][j] + m1.m[i][1] * m2.m[1][j] +
				m1.m[i][2] * m2.m[2][j] + m1.m[i][3] * m2.m[3][j];
		return r;
	}
	void printMatrix() {
		// clang-format off
		printf("[ %f, %f, %f, %f ] \n"
			"[ %f, %f, %f, %f ] \n"
			"[ %f, %f, %f, %f ] \n"
			"[ %f, %f, %f, %f ] \n",
			m[0][0], m[0][1], m[0][2], m[0][3],
			m[1][0], m[1][1], m[1][2], m[1][3],
			m[2][0], m[2][1], m[2][2], m[2][3],
			m[3][0], m[3][1], m[3][2], m[3][3]);
	}

	float m[4][4];
};

